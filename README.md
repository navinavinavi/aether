# Project aether

Pulling Selenium code out of almost nothing

How to use via commandline:

1. Install latest python (2.7 or 3.4)
2. Navigate to aether's home and do: pip install -r requirements.txt
3. Run: python aether.py your.json

Your JSON file should look like example.json, and the information included must be accurate.

Also included a navi4.json file to show what the most typical use-case will be.