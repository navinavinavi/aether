import json
import logging
from aether import main
from flask import Flask, request
from flask.ext.cors import CORS

app = Flask(__name__, static_folder='static')
cors = CORS(app)
logging.basicConfig()


@app.route('/aether', methods=['POST'])
def hello():
    data = json.loads(request.data.decode("utf-8"))
    filename = main(data['project_name'], data['login_url'], data['payload'], data['urls'], data['elements_package'],
                    data['actions_package'], data['selectors'])
    return filename


@app.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Chariot Server shutting down...'


def run_server():
    app.run(port=8002)


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


if __name__ == "__main__":
    run_server()