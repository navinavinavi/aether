import os
import re


class JavaElementsPrinter():
    def __init__(self, filepath, name, package):
        if not filepath.endswith("Elements.java"):
            raise ValueError("filepath parameter must end with Elements.java")

        # If the package name is None, raise ValueError, else give a placeholder
        if package is "":
            self.package = "replace.me"
        elif package is None:
            raise ValueError("elements_package must not be None")
        else:
            self.package = package

        self.name = name + "Elements"
        self.filepath = filepath

        # If the file in filepath exists, remove it
        try:
            os.remove(filepath)
        except OSError:
            pass
        self.write_new_file()

    def write_findbys(self, elements):
        # Create a regex filter for element attributes
        pattern = re.compile("[\W_]+")

        # Open the file and for every element, write a new WebElement assignment via Pagefactory annotations
        with open(self.filepath, "a") as f:
            for element_list in elements:
                for element in element_list:
                    if "id" in element.attrs:
                        f.write("\t@FindBy(id = \"%s\")\n\tpublic static WebElement %s%s;\n" %
                                (element["id"], element.name, re.sub(pattern, '', element["id"])))
                    elif "class" in element.attrs:
                        f.write("\t@FindBy(css = \"%s[class*='%s']\")\n\tpublic static WebElement %s%s;\n" %
                                (element.name, element["class"][0], element.name, re.sub(pattern, '', element["class"][0])))
                    elif "value" in element.attrs:
                        f.write("\t@FindBy(css = \"%s[value='%s']\")\n\tpublic static WebElement %s%s;\n" %
                                (element.name, element["value"], element.name, re.sub(pattern, '', element["value"])))
                    elif "name" in element.attrs:
                        f.write("\t@FindBy(css = \"%s[name='%s']\")\n\tpublic static WebElement %s%s;\n" %
                                (element.name, element["name"], element.name, re.sub(pattern, '', element["name"])))
                    else:
                        print("Couldn't find attr for %s" % element)

    def write_new_file(self):
        if not os.path.exists(os.path.dirname(self.filepath)):
            os.makedirs(os.path.dirname(self.filepath))
        with open(self.filepath, "a") as newfile:
            newfile.write("/*\n * This file was generated by aether\n*/\n\n"
                          "package %s;\n\n"
                          "import org.openqa.selenium.WebElement;\n"
                          "import org.openqa.selenium.support.FindBy;\n\n"
                          "public class %s {\n\n"
                          % (self.package, self.name))

    def write_end_file(self):
        with open(self.filepath, "a") as endfile:
            endfile.write("\n}")


class JavaStepsPrinter():
    def __init__(self, filepath, name, elements_package, steps_package):
        if not filepath.endswith("Steps.java"):
            raise ValueError("filepath parameter must end with Steps.java")

        # If the elements package name is None, raise ValueError
        if elements_package is None:
            raise ValueError("elements_package must not be None")
        else:
            self.elements_package = elements_package

        # If the steps package name is None, raise ValueError, else give a placeholder
        if steps_package is "":
            self.steps_package = "replace.me"
        elif steps_package is None:
            raise ValueError("steps_package must not be None")
        else:
            self.steps_package = steps_package

        self.name = name + "Steps"

        # Set elements name based on self.name
        self.elements_name = name + "Elements"
        self.filepath = filepath

        # If the file in filepath exists, remove it
        try:
            os.remove(filepath)
        except OSError:
            pass
        self.write_new_file()

    def write_steps(self, elements):
        # Create a regex filter for element attributes
        pattern = re.compile("[\W_]+")

        # Open the file and for every element, write a new placeholder method
        with open(self.filepath, "a") as f:
            for element_list in elements:
                for element in element_list:
                    if "id" in element.attrs:
                        f.write("\t@And(\"^Placeholder for %s.%s%s$\")\n"
                                "\tpublic void %s%s() {\n\t\t// Placeholder text\n\t}\n\n" %
                                (self.elements_name, element.name, re.sub(pattern, '', element["id"]), element.name,
                                 re.sub(pattern, '', element["id"])))
                    elif "class" in element.attrs:
                        f.write("\t@And(\"^Placeholder for %s.%s%s$\")\n"
                                "\tpublic void %s%s() {\n\t\t// Placeholder text\n\t}\n\n" %
                                (self.elements_name, element.name, re.sub(pattern, '', element["class"][0]), element.name,
                                 re.sub(pattern, '', element["class"][0])))
                    elif "value" in element.attrs:
                        f.write("\t@And(\"^Placeholder for %s.%s%s$\")\n"
                                "\tpublic void %s%s() {\n\t\t// Placeholder text\n\t}\n\n" %
                                (self.elements_name, element.name, re.sub(pattern, '', element["value"]), element.name,
                                 re.sub(pattern, '', element["value"])))
                    elif "name" in element.attrs:
                        f.write("\t@And(\"^Placeholder for %s.%s%s$\")\n"
                                "\tpublic void %s%s() {\n\t\t// Placeholder text\n\t}\n\n" %
                                (self.elements_name, element.name, re.sub(pattern, '', element["name"]), element.name,
                                 re.sub(pattern, '', element["name"])))
                    else:
                        print("Couldn't find attr for %s" % element)

    def write_new_file(self):
        if not os.path.exists(os.path.dirname(self.filepath)):
            os.makedirs(os.path.dirname(self.filepath))
        with open(self.filepath, "a") as newfile:
            newfile.write("/*\n * This file was generated by aether\n*/\n\n"
                          "package %s;\n\n"
                          "import %s.%s;\n"
                          "import cucumber.api.java.en.And;\n"
                          "import org.openqa.selenium.WebDriver;\n"
                          "import org.openqa.selenium.support.PageFactory;\n\n"
                          "public class %s {\n\n"
                          "\tprivate WebDriver driver;\n\n"
                          "\tpublic %s(WebDriver driver) {\n"
                          "\t\tPageFactory.initElements(driver, %s.class);\n"
                          "\t\tthis.driver = driver;\n"
                          "\t}\n\n"
                          % (self.steps_package, self.elements_package, self.elements_name, self.name, self.name,
                             self.elements_name))

    def write_end_file(self):
        with open(self.filepath, "a") as endfile:
            endfile.write("\n}")
