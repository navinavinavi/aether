'use strict';

// Declare app level module which depends on views, and components
angular.module('aether', ['ngRoute'])
    .config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/form', {
                    templateUrl: 'form/form.html',
                    controller: 'FormCtrl'
                })
                .when('/view', {
                    templateUrl: 'view/view.html',
                    controller: 'ViewCtrl'
                });
        }
    ])

    .controller('FormCtrl', function ($scope, $http) {
        $scope.data = {
            project_name: "default",
            elements_package: "default",
            actions_package: "default",
            login_url: "default",
            payload: '{\n  "username": "example",\n  "password": "test01"\n}',
            selectors: "#example",
            urls: '{\n  "Home": "http://example.com/index.html",\n  "Feature": "http://example.com/feature.html"\n}'
        };
        $scope.submitForm = function () {
            var payload_list = {};
            $('.paramkeys').each(function (k1, v1) {
                var keyval = $(this).val();
                $('.paramvalues').each(function (k2, v2) {
                    if(k1 == k2) {
                        payload_list[keyval] = $(this).val();
                        return true;
                    }
                })
            });

            var urls_list = {};
            $('.urlnames').each(function (k1, v1) {
                var keyval = $(this).val();
                $('.urls').each(function (k2, v2) {
                    if(k1 == k2) {
                        urls_list[keyval] = $(this).val();
                        return true;
                    }

                })
            });

            var selectors = [];
            $(".selectors").each(function () {
                selectors.push($(this).val());
            });

            var dataObject = {
                project_name: $scope.data.project_name,
                elements_package: $scope.data.elements_package,
                actions_package: $scope.data.actions_package,
                login_url: $scope.data.login_url,
                payload: payload_list,
                selectors: selectors,
                urls: urls_list
            };
            $scope.data.dataloading = true;
            $scope.data.datadone = false;
            $scope.data.dataerror = null;
            $http.post('http://localhost:8002/aether', JSON.stringify(dataObject)).
                success(function(data, status, headers, config) {
                    $scope.data.dataloading = false;
                    $scope.data.datadone = true;
                    JSZipUtils.getBinaryContent('static/' + data, function(err, data2) {
                        if(err) {
                            throw err; // or handle err
                        }

                        var zip = new JSZip(data2);
                        var content = zip.generate({type: "blob"});
                        saveAs(content, data);
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.data.dataloading = false;
                    $scope.data.dataerror = status;
                });
        };
        $scope.addSelector = function () {
            var selectorNum = $('#selectorList input').size() + 1;
            var new_select = $('<input ng-model="data.selector_' + selectorNum + '" class="form-control ng-valid ng-dirty selectors ng-valid-required" id="selector' + selectorNum + '" required />');
            $('#selectorList input:last-child').after(new_select);
        };
        $scope.addParameter = function () {
            var paramNum = $('#parameterKeyList input').size() + 1;
            var new_paramKey = $('<input ng-model="data.parameterkey_' + paramNum + '" class="form-control ng-valid ng-dirty paramkeys ng-valid-required" id="parameterKey' + paramNum + '" />');
            var new_paramValue = $('<input ng-model="data.parametervalue_' + paramNum + '" class="form-control ng-valid ng-dirty paramvalues ng-valid-required" id="parameterValue' + paramNum + '" />');
            $('#parameterKeyList input:last-child').after(new_paramKey);
            $('#parameterValueList input:last-child').after(new_paramValue);
        };
        $scope.addUrl = function () {
            var urlNum = $('#urlList input').size() + 1;
            var new_urlName = $('<input ng-model="data.urlName_' + urlNum + '" class="form-control ng-valid ng-dirty urlnames ng-valid-required" id="urlName' + urlNum + '" required />');
            var new_url = $('<input ng-model="data.url_' + urlNum + '" class="form-control ng-valid ng-dirty urls ng-valid-required" id="url' + urlNum + '" required />');
            $('#urlNameList input:last-child').after(new_urlName);
            $('#urlList input:last-child').after(new_url);
        };
    });
/*    .controller('ViewCtrl', function ($scope) {
        $scope.showContent = function ($fileContent) {
            $scope.content = $fileContent;
        };
    })

    .directive('onReadFile', function ($parse) {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element, attrs) {
                var fn = $parse(attrs.onReadFile);

                element.on('change', function (onChangeEvent) {
                    var reader = new FileReader();

                    reader.onload = function (onLoadEvent) {
                        scope.$apply(function () {
                            fn(scope, {$fileContent: onLoadEvent.target.result});
                        });
                    };

                    reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
                });
            }
        };
    });*/
