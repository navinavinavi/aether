import os
import requests
import shutil
from zipfile import ZipFile


from bs4 import BeautifulSoup
from printers import javaprinters


def main(project_name, login_url, payload, urls, elements_package, actions_package, selectors):
    # Set a temp Project path
    project_path = os.path.abspath("/tmp/%s" % project_name.replace("/", ""))
    if not os.path.exists(project_path):
        os.makedirs(project_path)

    # Start our session using "with" so it will automatically close
    with requests.Session() as session:
        session.post(login_url, data=payload)
        for name, url in urls.items():
            response = session.get(url)
            # Use BeautifulSoup to parse our page's body text
            soup = BeautifulSoup(response.text)

            # Create a list of elements based on selectors
            elements = [soup.select(css) for css in selectors]

            # Create Elements Class
            element_class = javaprinters.JavaElementsPrinter("%s/src/test/java/%s/%sElements.java" %
                                                             (project_path, elements_package.replace(".", "/"), name),
                                                             name, elements_package)
            element_class.write_findbys(elements)
            element_class.write_end_file()

            # Create Actions Class
            actions_class = javaprinters.JavaStepsPrinter("%s/src/test/java/%s/%sSteps.java" %
                                                          (project_path, actions_package.replace(".", "/"), name),
                                                          name, elements_package, actions_package)
            actions_class.write_steps(elements)
            actions_class.write_end_file()

    filename = "%s.zip" % project_name

    with ZipFile("aether/static/%s" % filename, 'w') as myzip:
        zipdir(project_path, myzip)

    shutil.rmtree(project_path)

    return filename


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


