module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'aether/bower_components/angular/angular.js',
      'aether/bower_components/angular-route/angular-route.js',
      'aether/bower_components/angular-mocks/angular-mocks.js',
      'aether/components/**/*.js',
      'aether/view*/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
